# Adonisjs-best-practices

## Contents

[1. Introduction](#introduction)

[2. Environment](#environment-variables)

[3. Follow AdonisJs naming conventions](#follow-adonisjs-naming-conventions)

[4. Validator](#validator)

[5. Database Hooks](#database-hooks)

[6. Refactoring search queries](#refactoring-search-queries)

[7. Edge Template](#edge-template)

[8. Eslint Integration](#eslint-integration)

# **Introduction**


[AdonisJs](https://adonisjs.com/) is a [NodeJs](https://nodejs.org/en/) MVC framework that runs on all major operating systems. It offers a stable eco-system to write a server-side web application so that you can focus on business needs over finalizing which package to choose or not.
The purpose of this article is to show the best practices to improve productivity for AdonisJs based business logics.

[🔝 Back to contents](#contents)


# **Environment Variables**


**Environment variables** are nice way to keep sensitive information out of the code base and easily configure the application. Therefore, refusing to write a piece of code can be turned into a nightmare by writing tens of lines of code.

* Environment variables solve various problems. Of course, environment variables are not belong only to AdonisJs. But AdonisJs has an ‘Env’ provider which is nice and elegant approach. 
While working on an AdonisJs project, you may want to have some variables that dynamically change according to the environment. For example, you may want to set the database connection settings, or maybe want to specify the applications URL. AdonisJs deals with this issue through an ‘.env’ file. This file allows you to keep different values for different environments (development, testing, production) that read by AdonisJs. This gives a very convenient way of manipulating the application’s behaviors according to environment.

```javascript
// Production .env
DB_CONNECTION=mysql
DB_HOST=prod_ip_addr
DB_PORT=3306
DB_USER=prod_user
DB_PASSWORD=prod_user_pass
DB_DATABASE=prod_db
// Development .env
DB_CONNECTION=mysql
DB_HOST=dev_ip_addr
DB_PORT=3306
DB_USER=dev_user
DB_PASSWORD=dev_user_pass
DB_DATABASE=dev_db
```

```javascript
// Create an 'Env' instance. 
const Env = use('Env');
// Get environment variables
let host =  Env.get('DB_HOST');
let port = Env.get('DB_PORT');
let user = Env.get('DB_USER');
let password = Env.get('DB_PASSWORD');
let db = Env.get('DB_DATABASE');
```


[🔝 Back to contents](#contents)


# **Follow AdonisJs naming conventions**


| What                             | How                                        | Good                                    | Bad                                      |
|----------------------------------|--------------------------------------------|-----------------------------------------|------------------------------------------|
| Controller                       | singular                                   | ArticleController                       | ~~ArticlesController~~                   |
| Route                            | plural                                     | articles/1                              | ~~article/1~~                            |
| Named route                      | snake_case with dot notation               | users.show_active                       | ~~users.show-active, show-active-users~~ |
| Model                            | singular                                   | User                                    | ~~Users~~                                |
| hasOne or belongsTo relationship | singular                                   | articleComment                          | ~~articleComments, article_comment~~     |
| All other relationships          | plural                                     | articleComments                         | ~~articleComment, article_comments~~     |
| Table                            | plural                                     | article_comments                        | ~~article_comment, articleComments~~     |
| Pivot table                      | singular model names in alphabetical order | article_user                            | ~~user_article, articles_users~~         |
| Table column                     | snake_case without model name              | meta_title                              | ~~MetaTitle; article_meta_title~~        |
| Foreign key                      | singular model name with _id suffix        | article_id                              | ~~ArticleId, id_article, articles_id~~   |
| Primary key                      | -                                          | id                                      | ~~custom_id~~                            |
| Migration                        | -                                          | 2017_01_01_000000_create_articles_table | ~~2017_01_01_000000_articles~~           |
| Method                           | camelCase                                  | getAll                                  | ~~get_all~~                              |

[🔝 Back to contents](#contents)


# **Validator**

* Install Validator In Adonis Js
```javascript
 adonis install @adonisjs/validator
```

* Then, register the validator provider inside the start/app.js file:

```javascript
const providers = [
  '@adonisjs/validator/providers/ValidatorProvider'
]
```



* AdonisJs allows us to easily verify the data via validators. Just determine the rules and AdonisJs will take care of the rest with the help of ‘Indicative’ which is a validation library. Don’t worry, the documents and API reference of Indicative is quite clear!

* You can write rules just like writing in English. Create a rules object with the validation rules you determine and execute the validation.

```javascript
const { validate } = use('Validator')
// Create rules object
const rules = {
email: 'required|email',
}
// Validate the data against to the rules.
const validation = await validate(data, rules)
// Check the fields that failed 
if (validation.fails()) {
// Code line
}
```

* **Route Validators**

    * Traditionally, data validation is performed at the beginning of the controller methods. Adonis introduces a new approach, called Route Validators, for data validation. Route validators make data validation a bit more easier and so code base gets more structured and efficiency and readability increases.
    Let’s create a validator that validates email saved for newsletter.

```javascript
adonis make:validator Newsletter
```

*  Adonis creates validators inside the app/Validators directory.

```javascript
// app/Validators/Newsletter
'use strict'
 
class Newsletter {
  get rules () {
    return {
      // validation rules
    }
  }
}
 
module.exports = Newsletter
```
* Change returned rule object as:

```javascript
return {
  email: 'required|email'
}
```

*  Chain the validator to the route:

```javascript
Route.post('/newsletter', 'EmailController.newsletter').validator('Newsletter')
```

*  Now, when a request hit to /newsletter route, the Newsletter route validator will be triggered and the data in the request object will be tested against the rules.
Hence, with validators, you can build your business logic in a more refined and elegant way.
For more information: [Validator](https://adonisjs.com/docs/4.1/validator)

[🔝 Back to contents](#contents)


# **DataBase Hooks**

* **Database Hooks** is a concept that is used to execute commands before or after a specific database operation. Hooks save you from writing same code over and over again.

* For example, for security reasons, you must save the user password into the database in **hashed** format. Using hooks, you can be sure that the user password will be hashed before each **creation.**

```javascript
class User extends Model {
  static boot() {
    super.boot();
    this.addHook('beforeCreate', async (user) => {
      user.password = await Hash.make(user.password);
    })
  }
}
```
*  Another example is that, again for security reasons, sometimes you may need to escape strings and in a such case, database hooks could be the best choice.

```javascript
const he = require('he');
class User extends Model {
  static boot() {
    super.boot();
    this.addHook('afterFetch', async (data) => {
      data.key = he.escape(data.key);
    })
  }
}
```

[🔝 Back to contents](#contents)


# **Refactoring search queries**

```javascript
const Post = use('App/Models/Post')

class PostsController {
    async index({ response, request }) {    
        const query = Post.query()

        if (request.input('category_id')) {
            query.where('category_id', request.input('category_id'))
        }

        let keyword = request.input('keyword')

        if (keyword) {
            keyword = `%${decodeURIComponent(keyword)}%`
            query
                .where('title', 'like', keyword)
                .orWhere('description', 'like', keyword)
        }

        const tags = request.input('tags')
        if (tags) {
            query.whereIn('tags', tags)
        }

        const posts = await query.where('active', true).fetch()

        return response.json({ posts: posts.toJSON() })
    }
}
```

*  **So let's dive into various ways we can clean this up.**

* **Scopes**
    * Adonis has a feature called [query scopes](https://adonisjs.com/docs/4.1/lucid#_query_scopes) that allows us to extract query constraints. Let's try this with the keyword constraint.

```javascript
keyword = `%${decodeURIComponent(keyword)}%`
query
    .where('title', 'like', keyword)
    .orWhere('description', 'like', keyword)
```
*    To create a new scope we would go into our Posts model and add the following method to the class

```javascript
scopeByEncodedKeyword(query, keyword) {
    keyword = `%${decodeURIComponent(keyword)}%`

    return query
        .where('title', 'like', keyword)
        .orWhere('description', 'like', keyword)
}
```

*    Now back in the controller, we can simply write

```javascript
if (keyword) {
    query.byEncodedKeyword(keyword)
}
```

* It's important that the method name is prefixed with scope. When calling scopes, drop the scope keyword and call the method in camelCase (ByEncodedKeyword => byEncodedKeyword).

* This is a great way to simplify queries and hide complexity! It also makes query constraints reusable.

[🔝 Back to contents](#contents)


# **Edge Template**

* Make sure that the AdonisJs ViewProvider is registered as a provider inside your start/app.js file.
* All views are stored in the resources/views directory and end with the .edge extension.

```javascript
const providers = [
  '@adonisjs/framework/providers/ViewProvider'
]
```

* Use the adonis command to create the view

```javascript
adonis make:view hello-world
```

**Style**

```javascript
{{ style('style') }}

//Absolute path

{{ style('https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css') }}

```

**Script**

```javascript
{{ script('app') }}

//Absolute path

{{ script('https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js') }}

```
**assetsUrl**
```javascript
<img src="{{ assetsUrl('images/logo.png') }}" />
```

**Globals scope**
* The value of this inside a global’s closure is bound to the view context so you can access runtime values from it

```javascript
View.global('button', function (text) {
  return this.safe(`<button type="submit">${text}</button>`)
})

```


# **Eslint Integration**

* Install Npm Package of ESLint

```javascript
npm i eslint
```

* package.json

```javascript
  "devDependencies": {
    ...
    "eslint": "^4.19.1",
    "eslint-config-airbnb-base": "^12.1.0",
    "eslint-plugin-import": "^2.9.0"
     ...
  }
```

* .eslintrc Create file if doesn't exits

```javascript
{
  "env": {
    "node": true
  },
  "parserOptions": {
    "ecmaVersion": 2017
  },
  "extends": "airbnb-base",
  "rules": {
    "strict": "off",
    "semi": ["error", "never"],
    "comma-dangle": ["error", "never"],
    "space-before-function-paren": ["error", { "anonymous": "never", "named": "always" }],
    "class-methods-use-this": "off",
    "object-curly-newline": ["error", { "multiline": true }],
    "global-require": "off",
    "arrow-parens": ["error", "as-needed"],
    "no-param-reassign": ["error", { "props": false }]
  },
  "globals": {
    "use": true
  }
}
```

* .eslintignore 
```javascript
public/   //Registered Ignored Directory or Files
```

[🔝 Back to contents](#contents)

